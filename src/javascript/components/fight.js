import { controls } from '../../constants/controls';

const {
    PlayerOneAttack,
    PlayerOneBlock,
    PlayerTwoAttack,
    PlayerTwoBlock
} = controls;



export async function fight(firstFighter, secondFighter) {

    return new Promise((resolve) => {
        // resolve the promise with the winner when fight is over
        const pressedKeys = {};

        window.onkeydown = function(event) { processAttack(event) };
        window.onkeyup = function(event) { pressedKeys[event.code] = false };

        let playerOneCanHitCritical = true;
        let playerTwoCanHitCritical = true;

        let leftPlayerHelthLine = document.getElementById("left-fighter-indicator");
        let rightPlayerHelthLine = document.getElementById("right-fighter-indicator");

        function processAttack(event) {
            pressedKeys[event.code] = true;

            let currHealthRightFighter = (rightPlayerHelthLine.style.width ? rightPlayerHelthLine.style.width : "100%").slice(0, -1);
            let currHealthLeftFighter = (leftPlayerHelthLine.style.width ? leftPlayerHelthLine.style.width : "100%").slice(0, -1);

            // Player One attacking
            if (event.code === PlayerOneAttack && currHealthRightFighter > 0 && !!pressedKeys[PlayerTwoBlock] === false && !!pressedKeys[PlayerOneBlock] === false) {

                let healthAfterDemage = (currHealthRightFighter - getDamage(firstFighter, secondFighter));
                if (healthAfterDemage < 0) {
                    resolveWinner(firstFighter, 'left', 'right');
                } else {
                    rightPlayerHelthLine.style.width = healthAfterDemage + "%";
                }
            }

            // Player One Special attack
            if (!!pressedKeys['KeyQ'] === true && !!pressedKeys['KeyW'] === true && !!pressedKeys['KeyE'] === true && playerOneCanHitCritical) {
                let healthAfterDemage = (currHealthRightFighter - firstFighter.attack * 2);
                if (rightPlayerHelthLine.style.width && healthAfterDemage < 0) {
                    resolveWinner(firstFighter, 'left', 'right');

                } else {
                    rightPlayerHelthLine.style.width = (currHealthRightFighter - firstFighter.attack * 2) + "%";
                    playerOneCanHitCritical = false;
                    setTimeout(() => {
                        playerOneCanHitCritical = true;
                    }, 10000);
                }
            }


            // Player Two attacking
            if (event.code === PlayerTwoAttack && currHealthLeftFighter > 0 && !!pressedKeys[PlayerOneBlock] === false && !!pressedKeys[PlayerTwoBlock] === false) {
                let healthAfterDemage = (currHealthLeftFighter - getDamage(secondFighter, firstFighter));
                if (healthAfterDemage < 0) {
                    resolveWinner(secondFighter, 'right', 'left');
                } else {
                    leftPlayerHelthLine.style.width = healthAfterDemage + "%";
                }
            }

            // Player Two Special attack
            if (!!pressedKeys['KeyU'] === true && !!pressedKeys['KeyI'] === true && !!pressedKeys['KeyO'] === true && playerTwoCanHitCritical) {
                let healthAfterDemage = (currHealthLeftFighter - secondFighter.attack * 2);

                if (leftPlayerHelthLine.style.width && healthAfterDemage < 0) {
                    resolveWinner(secondFighter, 'right', 'left');

                } else {
                    leftPlayerHelthLine.style.width = (currHealthLeftFighter - secondFighter.attack * 2) + "%";
                    playerTwoCanHitCritical = false;
                    setTimeout(() => {
                        playerTwoCanHitCritical = true;
                    }, 10000);
                }
            }
        }

        function resolveWinner(winnerFighter, winnerSide, loserSide) {

            let looserFighter = document.getElementsByClassName(`arena___${loserSide}-fighter`)[0];
            let fighterBodyElement = document.getElementsByClassName(`arena___${winnerSide}-fighter`)[0].firstElementChild;
            fighterBodyElement.className = `arena___${winnerSide}-fighter`;
            looserFighter.style.display = 'none';
            winnerFighter.bodyElement = fighterBodyElement;
            if (winnerSide === 'left') {
                rightPlayerHelthLine.style.width = 0;
            } else if (winnerSide === 'right') {
                leftPlayerHelthLine.style.width = 0;
            }
            return resolve(winnerFighter);
        };

    });
};


export function getDamage(attacker, defender) {
    // return damage
    let demage = getHitPower(attacker) - getBlockPower(defender);
    if (demage > 0) {
        return demage;
    } else return 0;
}

export function getHitPower(fighter) {
    // return hit power
    let attack = fighter.attack;
    let criticalHitChance = Math.random() * (2 - 1) + 1;
    let power = attack * criticalHitChance;

    return power;
}

export function getBlockPower(fighter) {
    // return block power
    let defense = fighter.defense;
    let dodgeChance = Math.random() * (2 - 1) + 1;
    let blockPower = defense * dodgeChance;

    return blockPower;
};