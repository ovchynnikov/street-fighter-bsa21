import { showModal } from './modal.js'
export function showWinnerModal(fighter, Element) {
    // call showModal function
    const winner = fighter.name.toUpperCase();
    const fighterBodyEl = fighter.bodyElement;
    showModal({ title: `   ${winner} is a WINNER!` + '🏆', bodyElement: fighterBodyEl });
};