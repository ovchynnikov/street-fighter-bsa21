import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
    const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
    const fighterElement = createElement({
        tagName: 'div',
        className: `fighter-preview___root ${positionClassName}`,
    });

    // todo:  show fighter info  (image, name, health, etc.)
    if (fighter) {

        const { attack, defense, health, name } = fighter;
        // console.log(attack, defense, health, name, source);

        const PlayerHealth = createElement({ tagName: 'span', className: 'arena___fighter-name' });
        PlayerHealth.innerText = `Health: ${health}`;

        const PlayerName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
        PlayerName.innerText = `Name: ${name}`;

        const PlayerAttack = createElement({ tagName: 'span', className: 'arena___fighter-name' });
        PlayerAttack.innerText = `Attack: ${attack}`;

        const PlayerDefense = createElement({ tagName: 'span', className: 'arena___fighter-name' });
        PlayerDefense.innerText = `Defense: ${defense}`;;

        const imgElement = createFighterImage(fighter);

        fighterElement.append(imgElement, PlayerName, PlayerHealth, PlayerDefense, PlayerAttack);

        return fighterElement;
    } else {
        return fighterElement;
    }
}

export function createFighterImage(fighter) {
    const { source, name } = fighter;
    const attributes = {
        src: source,
        title: name,
        alt: name
    };
    const imgElement = createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        attributes,
    });

    return imgElement;
}